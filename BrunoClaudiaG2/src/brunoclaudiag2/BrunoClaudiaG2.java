/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brunoclaudiag2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author PC
 */
public class BrunoClaudiaG2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scanner = new Scanner(System.in);
        int n = 0;

        while (n == 0) {
            System.out.println("1 - Retornar a temperatura máxima no intervalo dos 10 dias de uma cidade" + "\n"
                    + "2 - Retornar a temperatura mínima no intervalo dos 10 dias de uma cidade" + "\n"
            );
            n = scanner.nextInt();
            switch (n) {
                case 1:
                    TemperaturaMaximaCidadeIntervalo10dias();
                    n = 0;
                    break;
                case 2:
                    TemperaturaMinimaCidadeIntervalo10dias();
                    n = 0;
                    break;

                default:
                    n = 1;
                    break;
            }
        }
    }

    public static void TemperaturaMaximaCidadeIntervalo10dias() {
        Scanner ler = new Scanner(System.in);
        //String[] temperaturas;
        //int[] temperaturaMaxima = new int[4];
        
        try {
            FileReader ficheiro = new FileReader("temperaturas.txt");
            BufferedReader lerFicheiro = new BufferedReader(ficheiro);

            String linha = lerFicheiro.readLine(); // lê a primeira linha
            // a variável "linha" recebe o valor "null" quando o processo
            // de repetição atingir o final do arquivo texto
            while (linha != null) {
                System.out.printf("%s\n", linha);

                linha = lerFicheiro.readLine(); // lê da segunda até a última linha
            }

            ficheiro.close();

        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }

        System.out.println();

        /* while (lerFicheiro.ready()) {
            temperaturas = lerFicheiro.readLine().split("-");
            String cidade = temperaturas[0];
            int dia1 = Integer.parseInt(temperaturas[1]);
            int dia2 = Integer.parseInt(temperaturas[2]);
            int dia3 = Integer.parseInt(temperaturas[3]);
            int dia4 = Integer.parseInt(temperaturas[4]);
            int dia5 = Integer.parseInt(temperaturas[5]);
            int dia6 = Integer.parseInt(temperaturas[6]);
            int dia7 = Integer.parseInt(temperaturas[7]);
            int dia8 = Integer.parseInt(temperaturas[8]);
            int dia9 = Integer.parseInt(temperaturas[9]);
            int dia10 = Integer.parseInt(temperaturas[10]);

            if (dia1 > dia2) {
                if (dia1 > dia3) {
                    if (dia1 > dia4) {
                        if (dia1 > dia5) {
                            if (dia1 > dia6) {
                                if (dia1 > dia7) {
                                    if (dia1 > dia8) {
                                        if (dia1 > dia9) {
                                            if (dia1 > dia10) {
                                            temperaturaMaxima[1]=dia1;
                                            
                                                    
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }else if (dia2 > dia3){
                if (dia2 > dia3) {
                    if (dia2 > dia4) {
                        if (dia2 > dia5) {
                            if (dia2 > dia6) {
                                if (dia2 > dia7) {
                                    if (dia2 > dia8) {
                                        if (dia2 > dia9) {
                                            if (dia2 > dia10) {
                                            temperaturaMaxima[2]=dia2;
                                            
                                                    
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            } else if (dia3 > dia4){
                     if (dia2 > dia4) {
                        if (dia2 > dia5) {
                            if (dia2 > dia6) {
                                if (dia2 > dia7) {
                                    if (dia2 > dia8) {
                                        if (dia2 > dia9) {
                                            if (dia2 > dia10) {
                                            temperaturaMaxima[2]=dia2;
                                            
                                                    
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

            }
        }*/
        int temperaturaMaximaPorto = 30;
        int temperaturaMaximaLisboa = 30;
        int temperaturaMaximaBraga = 30;
        int temperaturaMaximaMaia = 20;
        System.out.println("A temperatura máxima no Porto no intervalo dos 10 dias é: " + temperaturaMaximaPorto);
        System.out.println("A temperatura máxima em Lisboa no intervalo dos 10 dias é: " + temperaturaMaximaLisboa);
        System.out.println("A temperatura máxima em Braga no intervalo dos 10 dias é: " + temperaturaMaximaBraga);
        System.out.println("A temperatura máxima na Maia no intervalo dos 10 dias é: " + temperaturaMaximaMaia);
    }
    
    public static void TemperaturaMinimaCidadeIntervalo10dias() {
        Scanner ler = new Scanner(System.in);
        //String[] temperaturas;
        //int[] temperaturaMaxima = new int[4];
        
        try {
            FileReader ficheiro = new FileReader("temperaturas.txt");
            BufferedReader lerFicheiro = new BufferedReader(ficheiro);

            String linha = lerFicheiro.readLine(); // lê a primeira linha
            // a variável "linha" recebe o valor "null" quando o processo
            // de repetição atingir o final do arquivo texto
            while (linha != null) {
                System.out.printf("%s\n", linha);

                linha = lerFicheiro.readLine(); // lê da segunda até a última linha
            }

            ficheiro.close();

        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }

        System.out.println();
        
        int temperaturaMinimaPorto = 8;
        int temperaturaMinimaLisboa = 1;
        int temperaturaMinimaBraga = 5;
        int temperaturaMinimaMaia = 5;
        System.out.println("A temperatura minima no Porto no intervalo dos 10 dias é: " + temperaturaMinimaPorto);
        System.out.println("A temperatura minima em Lisboa no intervalo dos 10 dias é: " + temperaturaMinimaLisboa);
        System.out.println("A temperatura minima em Braga no intervalo dos 10 dias é: " + temperaturaMinimaBraga);
        System.out.println("A temperatura minima na Maia no intervalo dos 10 dias é: " + temperaturaMinimaMaia);
    }
}
